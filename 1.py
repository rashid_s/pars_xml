import xml.etree.ElementTree as etree
import csv
e = etree.parse('2018.05.17_ЛОГ_ОВВ_HBM.xml').getroot()


all_rows = []
tmp = {'actual': None, 'prev': None}
tmp1 = {'actual': None, 'prev': None}
for atype in e.findall('item'):
    try:
        idx = (atype.get('idx'))
        adc = (atype.get('adc'))
        if adc == '0':
            continue
        tmp['actual'] = (atype.get('tmp'))
        tmp1['actual'] = (atype.get('tmp1'))
        if tmp['actual'] == '-273':
            tmp['actual'] = tmp['prev']
        else:
            tmp['prev'] = tmp['actual']
        tmp['actual'] = tmp['actual'].replace('.', ',')
        if tmp1['actual'] == '-273':
            tmp1['actual'] = tmp1['prev']
        else:
            tmp1['prev'] = tmp1['actual']
        tmp1['actual'] = tmp1['actual'].replace('.', ',')
        row = (idx, adc, tmp['actual'], tmp1['actual'])
        all_rows.append(row)
    except Exception:
        pass


with open('2018.05.17_ЛОГ_ОВВ_HBM.csv', 'w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=';')
    for item in all_rows:
        csv_writer.writerow(item)
